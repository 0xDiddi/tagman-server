import chokidar from 'chokidar';
import express from 'express';
import path from 'path';
import ThumbnailGenerator from 'video-thumbnail-generator';
import TagManager from './TagManager';

interface FsConfig {
    basePath: string,
    usePolling: boolean,
    pollingInterval: number,
}

class FS {
    private Path: string;
    private watcher: chokidar.FSWatcher;
    private tagManager: TagManager;

    public Router: express.Router;

    private initialScanActive: boolean;
    private initialScanList: string[];

    constructor(config: FsConfig, tags: TagManager) {
        this.Path = path.resolve(config.basePath);
        this.tagManager = tags;

        this.initialScanActive = true;
        this.initialScanList = [];

        this.watcher = chokidar.watch(this.Path, {
            usePolling: config.usePolling || false,
            interval: config.pollingInterval || 1000,
            binaryInterval: config.pollingInterval || 1000,
        });
        this.watcher.on('add', this.OnFileChanged(false));
        this.watcher.on('unlink', this.OnFileChanged(true));
        this.watcher.on('ready', () => {
            this.initialScanActive = false;
            this.tagManager.pushInitialFiles(this.initialScanList);
            console.info("items: " + this.initialScanList.length);
        });

        this.Router = express.Router();
        this.Router.use('/', express.static(this.Path));

        this.Router.use('/t/', express.static('./thumbs'))
    }

    private OnFileChanged(removed: boolean): (path: string) => void {
        return (file: string): void => {
            // generate thumbnails for videos and gifs
            // todo: find a way to handle huge volume on startup
            if (!this.initialScanActive && file.endsWith('.mp4') || file.endsWith('.webm') || file.endsWith('.gif')) {
                let tg = new ThumbnailGenerator({
                    sourcePath: file,
                    thumbnailPath: './thumbs/',
                });
                tg.generate({
                    count: 1,
                    size: '576x324',
                    timemarks: ['00:00:01.000'],
                    filename: `%b`,
                });
            }

            file = path.relative(this.Path, file);
            // replace backslash dir separators on windows systems with forward slash
            file = file.replace(new RegExp('\\\\', 'g'), '/');
            console.info(`${file} has been ${removed ? 'removed' : 'added'}`);

            if (this.initialScanActive) {
                // during the initial scan, we don't get events for removal, so no need to  save that
                this.initialScanList.push(file);
            } else this.tagManager.updateFile(file, removed);
        };
    }
}

export = FS;
