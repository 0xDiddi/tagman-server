class NetError extends Error {
    public readonly code: number;
    public readonly msg: string;

    constructor(code: number, msg: string) {
        super(`Error ${code}: ${msg}`);

        this.code = code;
        this.msg = msg;
    }

    public toJSON() {
        return {
            code: this.code,
            message: this.msg,
        };
    }
}

export = NetError;
